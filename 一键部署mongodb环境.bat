@echo off
chcp 65001
echo "当前目录"
cd %~dp0
echo "输出当前目录"
echo %cd%
echo "dns 查询 https://ping.chinaz.com/fastdl.mongodb.org 若下载速度有所缓慢可以自行查询dns ip地址"
echo "将curl命令修改即可 fastdl.mongodb.org:443:18.65.168.42"
if not exist "mongodb.zip" (
    echo "mongodb.zip not found"
    curl --resolve "fastdl.mongodb.org:443:18.65.168.42" -L -o mongodb.zip https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-4.0.28.zip
    echo "mongodb.zip downloaded"
)
if not exist "mongodb" (
    echo "mongodb folder not exists"
    echo "mongodb.zip extracted"
    powershell Expand-Archive -Path mongodb.zip -DestinationPath .
    echo "changing folder name"
    move mongodb-win32-x86_64-2008plus-ssl-4.0.28 mongodb
)
echo "创建数据目录cache"
if not exist "cache" (
    mkdir cache
)
echo "创建日志目录cache\log"
if not exist "cache\log" (
    mkdir cache\log
)
echo "检查配置文件cache\mongodb.conf"
if not exist "cache/mongodb.conf" (
    echo "创建配置文件cache/mongodb.conf"
    echo systemLog: > cache/mongodb.conf
    echo     destination: file >> cache/mongodb.conf
    echo     path: cache/log/mongodb.log >> cache/mongodb.conf
    echo     logAppend: true >> cache/mongodb.conf
    echo storage: >> cache/mongodb.conf
    echo     dbPath: cache >> cache/mongodb.conf
    echo     journal: >> cache/mongodb.conf
    echo         enabled: true >> cache/mongodb.conf
    echo net: >> cache/mongodb.conf
    echo     bindIp: 127.0.0.1 >> cache/mongodb.conf
    echo     port: 27017 >> cache/mongodb.conf
)
echo "隐藏cache目录"
attrib +h cache
echo "隐藏mongodb目录"
attrib +h mongodb
echo "查询mongodb进程"
tasklist | findstr "mongod.exe"
if %errorlevel% neq 0 (
    echo "后台启动mongodb"
    start /b mongodb\bin\mongod.exe --config cache\mongodb.conf
)
echo "查询mongodb进程"
tasklist | findstr "mongod.exe"
echo "mongodb启动成功"








