# mongodb_environment

#### 1、Windows平台一键部署启动mongodb4.0版本
#### 2、已经指定了最快的dns服务器地址，下载速度会比较快
#### 3、如果下载速度慢，可以自行修改dns
```commandline
dns 查询 https://ping.chinaz.com/fastdl.mongodb.org
若下载速度有所缓慢可以自行查询dns ip地址
将curl命令修改即可 fastdl.mongodb.org:443:18.65.168.42
```
#### 4、指定了 项目目录下 cache 作为mongodb的数据存储目录
#### 5、指定了 项目目录下 cache\log 作为mongodb的日志存储目录
#### 6、运行脚本后，会自动下载mongodb4.0.4版本，解压并通过后台方式启动mongodb服务

#### 如果您对我的项目感兴趣，可以点击右上角的star，谢谢您的支持！
#### 如果您有什么问题，可以通过以下方式联系我，我会尽快回复您！

## 联系我

<img alt="./images/mmqrcode1712278058030.png" height="270" src="./images/mmqrcode1712278058030.png" title="联系我" width="200"/>